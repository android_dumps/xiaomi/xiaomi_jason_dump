#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:55665998:01f2599b7c36edfaf09642555c11334e2422e5fa; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:51250506:fcc46317959bf4b81949be61d89ded0a09f1342d EMMC:/dev/block/bootdevice/by-name/recovery 01f2599b7c36edfaf09642555c11334e2422e5fa 55665998 fcc46317959bf4b81949be61d89ded0a09f1342d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
